import { render, screen } from '@testing-library/react';
import Plugin from './index';

describe('测试 Plugin1', () => {
  it('第一步，渲染插件', () => {
    render(<Plugin />);
    const linkElement = screen.getByText(/Plugin Example/i);
    expect(linkElement).toBeInTheDocument();
  });
  it('第二步', () => {});
  it('第三步', () => {});
  it('第四步', () => {});
  // ...
});
