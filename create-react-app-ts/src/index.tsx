import React from 'react';
import ReactDOM from 'react-dom';
import Example from '../src/Example';

ReactDOM.render(
  <React.StrictMode>
    <Example />
  </React.StrictMode>,
  document.getElementById('root')
);
