import React from 'react'
import ReactDOM from 'react-dom'
import './main.less'

ReactDOM.render(
  <React.StrictMode>
    <h1>vite-React-Typescript-Template</h1>
  </React.StrictMode>,
  document.getElementById('root'),
)
