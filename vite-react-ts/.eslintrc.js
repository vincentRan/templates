module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es2021: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
    'prettier',
  ],
  parserOptions: {
    ecmaVersion: 12,
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: ['@typescript-eslint', 'prettier'],
  rules: {
    'comma-dangle': [2, 'always-multiline'], // 末尾逗号
    'no-var': 'error',
    'prefer-spread': 0,
    'no-useless-escape': 0,
    'no-debugger': 1,
    'no-console': 1,

    allowEmptyCatch: 'off',
    'prettier/prettier': 'error',
    'prefer-const': 'error',
    '@typescript-eslint/no-unused-vars': 1,
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/ban-ts-comment': 'off',
    'no-empty': ['error', { allowEmptyCatch: true }],
    // ============================================================ old
    'no-underscore-dangle': 0,
    'import/extensions': ['off', 'never'],
    'space-before-function-paren': 0,
    indent: ['error', 2, { SwitchCase: 1 }],
    'class-methods-use-this': 0,

    'no-bitwise': ['error', { allow: ['~'] }],
    'import/no-dynamic-require': ['off'],
    'prefer-destructuring': 0,
    'prefer-rest-params': 0,
    'no-param-reassign': 0,
    'no-mixed-operators': 0,
    'no-restricted-globals': 0,
    'import/no-extraneous-dependencies': 0,
    'no-continue': 0,
    'no-unused-expressions': 0,
    'max-len': 0,
  },
  globals: {
    defineProps: 'readonly',
    defineEmits: 'readonly',
    defineExpose: 'readonly',
    withDefaults: 'readonly',
  },
}
