import { defineConfig } from 'vite'
import { resolve } from 'path'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    strictPort: true, // 端口号自增
    // port: 8088,
    open: true,
  },
  build: {
    // 自定义构建目录
    // outDir: '../lampow-bridge/public/',
    chunkSizeWarningLimit: 300,
    minify: 'terser',
    terserOptions: {
      compress: {
        drop_console: true, // 移除 console
        drop_debugger: true,
      },
    },
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src'),
    },
  },
  css: {
    preprocessorOptions: {
      less: {
        // 全局less变量、函数
        // additionalData: `@import "${resolve(__dirname, 'src/global.less')}";`,
        modifyVars: {
          // 用于覆盖 antd 样式
          // 'primary-color': '#1890ff', // 全局主色 #1890ff
          // 'link-color': #1890ff; // 链接色
          // 'success-color': #52c41a; // 成功色
          // 'warning-color': #faad14; // 警告色
          // 'error-color': #f5222d; // 错误色
          // 'font-size-base': 14px; // 主字号
          // 'heading-color': rgba(0, 0, 0, 0.85); // 标题色
          // 'text-color': 'rgba(0, 0, 0, 0.65)', // 主文本色 rgba(0, 0, 0, 0.65)
          // 'text-color-secondary': rgba(0, 0, 0, 0.45); // 次文本色
          // 'disabled-color': rgba(0, 0, 0, 0.25); // 失效色
          // 'border-radius-base': 2px; // 组件/浮层圆角
          // 'border-color-base': #d9d9d9; // 边框色
          // 'box-shadow-base': 0 3px 6px -4px rgba(0, 0, 0, 0.12), 0 6px 16px 0 rgba(0, 0, 0, 0.08), 0 9px 28px 8px rgba(0, 0, 0, 0.05); // 浮层阴影
        },
        // 修复ant报错
        javascriptEnabled: true,
      },
    },
  },
})
