module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es2021: true
  },
  parser: 'vue-eslint-parser',
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
    'prettier'
  ],
  parserOptions: {
    ecmaVersion: 12,
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true
    }
  },
  plugins: ['vue', '@typescript-eslint', 'prettier'],
  rules: {
    'no-var': 'error',
    allowEmptyCatch: 'off',
    'prettier/prettier': 'error',
    'prefer-const': 'error',
    '@typescript-eslint/no-unused-vars': 'error',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/ban-ts-comment': 'off',
    'no-empty': ['error', { allowEmptyCatch: true }],
    'vue/no-unused-vars': [
      'error',
      {
        ignorePattern: '^_'
      }
    ],
    'vue/multi-word-component-names': 'off',
    'vue/no-parsing-error': [
      'error',
      {
        'control-character-reference': false
      }
    ],
    'vue/no-v-html': 'off',

    // ============================================================ old vue
    quotes: [
      2,
      'single',
      {
        avoidEscape: true,
        allowTemplateLiterals: true
      }
    ],
    'import/no-unresolved': ['off'],
    'no-underscore-dangle': 0,
    'global-require': 0,
    'import/extensions': ['off', 'never'],
    'comma-dangle': ['error', 'only-multiline'],
    'space-before-function-paren': 0,
    indent: ['error', 2, { SwitchCase: 1 }],
    'class-methods-use-this': 0,
    'vue/html-closing-bracket-newline': ['off'],
    'vue/multiline-html-element-content-newline': ['off'],
    'vue/singleline-html-element-content-newline': ['off'],
    'vue/html-closing-bracket-spacing': ['off'],
    'vue/html-indent': ['off'],
    'vue/attributes-order': ['off'],
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'pro' ? 2 : 0,
    'no-console': process.env.NODE_ENV === 'pro' ? 0 : 0,
    'no-alert': 0,
    'no-bitwise': ['error', { allow: ['~'] }],
    'import/no-dynamic-require': ['off'],
    'prefer-destructuring': 0,
    'prefer-rest-params': 0,
    'no-param-reassign': 0,
    'no-mixed-operators': 0,
    'no-restricted-globals': 0,
    'import/no-extraneous-dependencies': 0,
    'no-continue': 0,
    'func-names': 0,
    'no-unused-expressions': 0,
    'max-len': 0
  },
  globals: {
    defineProps: 'readonly',
    defineEmits: 'readonly',
    defineExpose: 'readonly',
    withDefaults: 'readonly'
  }
}
