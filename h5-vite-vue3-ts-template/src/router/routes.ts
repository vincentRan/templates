import { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'home',
    meta: {
      type: 'home'
    },
    component: () => import('@/views/home')
  }
]
export default routes
