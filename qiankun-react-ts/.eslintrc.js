module.exports = {
  extends: ['react-app', 'react-app/jest', 'plugin:prettier/recommended'],
  rules: {
    'no-debugger': process.env.REACT_APP_ENV === 'pro' ? 2 : 0,
    'no-console': process.env.REACT_APP_ENV === 'pro' ? 2 : 0,
    'react/react-in-jsx-scope': 0,
    '@typescript-eslint/no-explicit-any': 0,
    '@typescript-eslint/ban-ts-comment': 0,
    'react/jsx-key': 0,
    '@typescript-eslint/ban-types': 0,
    'prettier/prettier': [
      'error',
      {
        semi: false, // 句尾分号
        singleQuote: true, // 使用单引号
      },
    ],
  },
}
