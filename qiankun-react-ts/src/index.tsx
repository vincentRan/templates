import React from 'react'
import ReactDOM from 'react-dom'
import ReactDOMClient from 'react-dom/client'
import './index.less'

const dom = document.getElementById('root') as HTMLElement
console.log(1)

const render = () => {
  const root = ReactDOMClient.createRoot(dom)
  root.render(
    <React.StrictMode>
      <h1># react-ts-template webpack</h1>
    </React.StrictMode>
  )
}

// @ts-ignore
if (!window.__POWERED_BY_QIANKUN__) {
  render()
}

export async function bootstrap() {
  console.log('加载')
}
export async function mount() {
  render()
}
export async function unmount() {
  ReactDOM.unmountComponentAtNode(dom)
}
