/* eslint-disable */

const path = require('path')
const paths = require('react-scripts/config/paths')
const {
  override,
  addWebpackAlias,
  addLessLoader,
  overrideDevServer,
  addWebpackModuleRule,
} = require('customize-cra')

const isDev = process.env.REACT_APP_ENV === 'dev'

// 配置
const { buildPath, assetPath } = {
  buildPath: '../lampow-bridge/public/', // 构建路径
  assetPath: 'http://localhost:7000/', // 资源路径
}

// 自定义构建路径
paths.appBuild = path.resolve(path.dirname(paths.appBuild), buildPath)

module.exports = {
  webpack: override(
    // less 模块用法
    addLessLoader({
      lessOptions: {
        javascriptEnabled: true,
        // localIdentName: '[local]--[hash:base64:5]',
        cssModules: {
          localIdentName: '[local]--[hash:base64:5]', // if you use CSS Modules, and custom `localIdentName`, default is '[local]--[hash:base64:5]'.
        },
      },
    }),
    // 路径别名
    addWebpackAlias({
      '@': path.resolve(__dirname, './src'),
    }),
    // rule
    addWebpackModuleRule({
      test: /\.less$/,
      use: [
        'style-loader',
        'css-loader',
        'less-loader',
        {
          loader: 'style-resources-loader',
          options: {
            // 全局引入公共 less 文件
            patterns: path.resolve(__dirname, './src/variable.less'),
          },
        },
      ],
    }),
    // 默认入口
    (config) => {
      config.output.library = 'reactApp'
      config.output.libraryTarget = 'umd'
      if (isDev) config.output.publicPath = assetPath

      return config
    }
  ),
  devServer: overrideDevServer(() => (config) => {
    return {
      ...config,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
    }
  }),
}
