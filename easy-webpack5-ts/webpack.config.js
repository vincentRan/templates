const path = require("path");
const TerserPlugin = require("terser-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = ({ pro: isPro }) => {
  const plugins = [
    new HtmlWebpackPlugin({
      title: "测试webpack构建出html",
      template: "./index.html",
      // filename: "./html/index.html",
      // //指定引入js文件
      // chunks: ["index"],
      // //压缩处理
      // minify: {
      //   //压缩空格
      //   collapseWhitespace: true,
      //   //移出注释
      //   removeComments: true,
      // },
    }),
    isPro ? new CleanWebpackPlugin() : "",
  ].filter((n) => n);

  return {
    entry: "./src/man",
    output: {
      filename: "[name]__[contenthash:8].js",
      path: path.resolve(__dirname, "dist"),
    },
    resolve: {
      extensions: [".ts", ".js"],
    },
    devtool: "inline-source-map",
    // watch配置
    watchOptions: {
      ignored: /node_modules/, // 忽略目录，支持正则匹配
      aggregateTimeout: 500, // 500ms 防抖（默认 300ms）
      poll: 1500, // 轮询变化（默认 1000ms）
    },
    // 本地服务
    devServer: {
      static: "./dist", // 资源路径
      port: 9176, // 端口
      compress: true, // 压缩 .gzip
      open: true, // 启动时自动打开浏览器
      hot: true, // 热更新
    },
    // 压缩配置
    optimization: {
      minimize: true,
      minimizer: [
        new TerserPlugin({
          extractComments: false, // 去除编译后生成的 LICENSE.txt
        }),
      ],
    },
    // 构建后文件大小限制
    performance: {
      hints: "warning", // 枚举
      hints: "error", // 性能提示中抛出错误
      hints: false, // 关闭性能提示
      maxAssetSize: 200000, // 整数类型（以字节为单位）
      maxEntrypointSize: 400000, // 整数类型（以字节为单位）
      assetFilter: function (assetFilename) {
        // 提供资源文件名的断言函数:css js 资源大小
        return assetFilename.endsWith(".css") || assetFilename.endsWith(".js");
      },
    },
    // 插件
    plugins,
    module: {
      rules: [
        { test: /\.tsx?$/, loader: "ts-loader" },
        // {
        //   test: /\.css$/,
        //   sideEffects: true,
        //   loader: "css-loader",
        // },
      ],
    },
  };
};
