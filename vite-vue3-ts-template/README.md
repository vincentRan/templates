> ## 本模版特色：
>
> <hr/>

### 1. `Vue3` + `TypeScript` + `Vite`

### 2. 完美支持 `TSX`

### 3. `postcss` + `eslint` + `prettier` + `husky` + `commitlint`

### 4. 完美适配 viewport 的 `vw` 布局，支持各端，包括 pc 的 browser 端

### 5. 完美支持 `@` 别名，包括 `ts提示` 以及 `智能路径提示`（扩展别名需要同步更新 tsconfig，.vscode）

### 6. 支持全局 `单less变量` 的配置以及全局 `less变量文件`

### 7. `json` 文件支持注释

> ## 快速开始
>
> <hr/>

## 1. 确认 `node>=14.0.0` 版本

## 2. `npm i` && `yarn`(推荐) 安装依赖

## 2. `npm run dev ` && `yarn dev`(推荐) 进入开发模式

> ## 暂未集成
>
> <hr/>

### axois api 封装

### vuex 全局状态封装
