import { defineComponent } from 'vue'
import { RouterView } from 'vue-router'
import '@/style/base.less'

export default defineComponent({
  setup() {
    return () => <RouterView />
  }
})
