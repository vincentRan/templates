import { resolve } from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'

import pkg from './package.json'
import postcss from './.postcss'

const ENV = process.env.VUE_APP_ENV || 'dev'

export default defineConfig({
  // 基础访问路径以及资源请求路径
  base: ENV === 'dev' ? '/' : `/aa/${ENV}/bb/`,
  plugins: [vue(), vueJsx()],
  server: {
    open: true
  },
  build: {
    // 打包路径
    outDir: `dist/${ENV}/${pkg.version}/`
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src')
    }
  },
  css: {
    postcss,
    preprocessorOptions: {
      less: {
        // 全局less变量文件
        // additionalData: `@import "${resolve(__dirname, 'src/global.less')}";`,
        // 单个变量
        // modifyVars: {
        // 'primary-color': '#1890ff', // 全局主色
        // },
        // javascriptEnabled: true
      }
    }
  }
})
