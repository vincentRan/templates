#!/usr/bin/env node
const chokidar = require('chokidar')
const shell = require('shelljs')

const watcher = (event, path) => {
  console.log(event, path)
  if (event !== 'change') return
  shell.exec('yarn build')
}

chokidar.watch('index.ts').on('all', watcher)
