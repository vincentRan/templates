#!/usr/bin/env node
const fs = require('fs')
const path = require('path')
const shell = require('shelljs')

const pkgPath = path.resolve(__dirname, '../package.json')

// 升级版本号
shell.exec('git add .;git commit -m:"npm deploy"')
shell.exec('npm version prerelease')

// 保存 package.json
shell.exec('rm -rf .temp/')
shell.exec('mkdir .temp')
shell.exec('cp package.json .temp/package.json')

// 读取 package.json
const pkg = JSON.parse(fs.readFileSync(pkgPath))

// 删除不必要的字段
delete pkg.scripts
delete pkg.devDependencies

// 写入新 package.json
fs.writeFileSync(pkgPath, JSON.stringify(pkg, null, 2))

// 发布
shell.exec('npm publish', (error) => {
  // 覆盖不提示
  shell.exec('cp -rf .temp/package.json package.json')

  if (error) {
    shell.exec('git reset HEAD~')
  }
})
